const request = require('supertest')
const app = require('./app')

describe('App test', () => {
  it('has the default page', (done) => {
    request(app)
      .get('/')
      .expect(200)
      .expect(/Hello Express!/, done)
  })
})
