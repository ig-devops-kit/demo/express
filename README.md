# Express
Express - Hello Express 애플리케이션

#### 환경
- Express version 4.18.2
- eslint: 8.28.0
- jest: 29.2.2
- supertest: 6.3.1

#### 파이프라인 정보

- **Node.js Basic**
<!-- [node:build, node:lint, node:test] -->
- **Express with Docker Compose**
<!-- [node:build, node:lint, node:test, security:semgrep-sast, security:code-quality, security:secret_detection, security:container_scanning, docker:kaniko, deploy:docker-compose] -->


